
const cookieBox = document.querySelector(".wrapper"),
acceptBtn = cookieBox.querySelector("button");
acceptBtn.onclick = ()=>{
  //Aceptar cookies y que caduquen
  document.cookie = "PiMaps=Cookie; max-age="+60*60*24*30;
  
  if(document.cookie){ //if cookie is set
    cookieBox.classList.add("hide"); //hide cookie box
  
  }
}

let checkCookie = document.cookie.indexOf("CookieBy=CodingNepal");

checkCookie != -1 ? cookieBox.classList.add("hide") : cookieBox.classList.remove("hide");